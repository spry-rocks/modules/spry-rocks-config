import {Config, IConfigFileReader} from '@spryrocks/config-core';

export type Resources = {[key: string]: Config | undefined};

export class ReactConfigFileReader implements IConfigFileReader {
  constructor(private readonly resources: Resources) {}

  read(file: string): Config {
    try {
      return this.resources[file] || {};
    } catch (e) {
      return {};
    }
  }
}
