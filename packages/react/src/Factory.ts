import {ReactConfigFileReader, Resources} from './ReactConfigFileReader';
import {ConfigService} from '@spryrocks/config-core';

export const createConfigService = (
  build: string,
  app: string | undefined,
  platform: string | undefined,
  test: boolean,
  resources: Resources,
) =>
  new ConfigService(
    build,
    app,
    platform,
    test,
    undefined,
    new ReactConfigFileReader(resources),
  );

export const getBuildType = (nodeEnv?: string) =>
  (nodeEnv ?? process.env.REACT_APP_NODE_ENV ?? 'development').toLowerCase();

export const getAppType = () => {
  throw new Error('Not implemented yet!');
};
