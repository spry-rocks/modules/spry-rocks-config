import {
  createConfigService,
  getBuildType,
  IConfigService,
  Resources,
} from '@spryrocks/config-react';
import {ContainerModule} from 'inversify';
import {Platform} from 'react-native';
import RNBuildConfig from 'react-native-build-config';

export const createModule = (options?: {
  resources?: Resources;
  configService?: {constantValue: IConfigService};
}) =>
  new ContainerModule((bind) => {
    const getEnv = (): {[k: string]: string | undefined} => {
      if (Platform.OS === 'web') {
        return process.env;
      }
      return RNBuildConfig;
    };
    const getPlatform = () => Platform.OS;
    const getBuild = () => getEnv().Build;
    const getTestBuild = () => getEnv().TestBuild;
    const getApp = () => getEnv().App;
    if (options?.configService?.constantValue) {
      bind(IConfigService).toConstantValue(options.configService.constantValue);
    } else {
      bind(IConfigService)
        .toDynamicValue(() => {
          return createConfigService(
            getBuildType(getBuild()),
            getApp(),
            getPlatform(),
            !!getTestBuild(),
            options?.resources ?? {},
          );
        })
        .inSingletonScope();
    }
  });
