export type Config = {[key: string]: string | undefined};

export default interface IConfigFileReader {
  read(file: string): Config;
}
