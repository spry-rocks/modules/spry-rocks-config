import IConfigFileReader, {Config} from './IConfigFileReader';
import IConfigService from './IConfigService';

function dimensions(...dimensions: (string | undefined)[]) {
  return dimensions.filter((d) => !!d).map((d) => d as string);
}

function generateInternal(
  dimensions: string[],
  files: string[],
  base: string[] = ['env'],
) {
  files.push(`.${base.join('.')}`);
  for (let index = 0; index < dimensions.length; index++) {
    generateInternal(dimensions.slice(index + 1), files, [...base, dimensions[index]]);
  }
}

export default class ConfigService implements IConfigService {
  private readonly config: Config;

  constructor(
    build: string,
    app: string | undefined,
    platform: string | undefined,
    test: boolean,
    baseConfig: Config | undefined,
    configFileReader: IConfigFileReader,
  ) {
    const files: string[] = [];
    generateInternal(
      dimensions(build, app, platform, test ? 'test' : undefined, 'local'),
      files,
    );
    this.config = ConfigService.readConfig(files, baseConfig, configFileReader);
  }

  get(key: string, defaultValue?: string): string {
    return ConfigService.requireValue(key, this.getOptional(key), defaultValue);
  }

  getOptional(key: string): string | undefined {
    return this.config[key];
  }

  getNumber(key: string, defaultValue?: number): number {
    return ConfigService.requireValue(key, this.getNumberOptional(key), defaultValue);
  }

  getNumberOptional(key: string): number | undefined {
    const valueS = this.getOptional(key);
    if (!valueS) return undefined;
    const value = Number(valueS);
    if (Number.isNaN(value))
      throw new Error(ConfigService.formatEnvError(key, 'is not a number'));
    return value;
  }

  getBoolean(key: string, defaultValue?: boolean): boolean {
    return ConfigService.requireValue(key, this.getBooleanOptional(key), defaultValue);
  }

  getBooleanOptional(key: string): boolean | undefined {
    const valueS = this.getOptional(key);
    if (!valueS) return undefined;
    switch (valueS) {
      case 'true':
        return true;
      case 'false':
        return false;
      default:
        throw new Error(ConfigService.formatEnvError(key, 'is not a boolean'));
    }
  }

  private static readConfig(
    files: string[],
    baseConfig: Config | undefined,
    configFileReader: IConfigFileReader,
  ): Config {
    let config = baseConfig || {};
    for (const file of files) {
      config = {
        ...config,
        ...configFileReader.read(file),
      };
    }
    return config;
  }

  private static formatEnvError(key: string, errorMessage: string) {
    return `env ${key} ${errorMessage}`;
  }

  private static requireValue<TValue>(
    key: string,
    value: TValue | undefined,
    defaultValue: TValue | undefined,
  ): TValue {
    if (value === undefined) {
      if (defaultValue !== undefined) return defaultValue;
      throw new Error(ConfigService.formatEnvError(key, 'not found'));
    }
    return value;
  }
}
