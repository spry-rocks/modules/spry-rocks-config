import * as dotenv from 'dotenv';
import * as fs from 'fs';
import {Config, IConfigFileReader} from '@spryrocks/config-core';

export class NodeConfigFileReader implements IConfigFileReader {
  // eslint-disable-next-line class-methods-use-this
  read(file: string): Config {
    try {
      if (!fs.existsSync(file)) return {};
      return dotenv.parse(fs.readFileSync(file));
    } catch (e) {
      return {};
    }
  }
}
