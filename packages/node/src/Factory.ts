import {Config, ConfigService} from '@spryrocks/config-core';
import {NodeConfigFileReader} from './NodeConfigFileReader';

const getBaseConfig = (): Config => process.env;

export const createConfigService = (
  build: string,
  app: string | undefined,
  test: boolean,
) =>
  new ConfigService(
    build,
    app,
    undefined,
    test,
    getBaseConfig(),
    new NodeConfigFileReader(),
  );

export const getConfigEnv = (): string => process.env.NODE_ENV || 'development';

export const getConfigApp = () => {
  throw new Error('Not implemented yet!');
};
